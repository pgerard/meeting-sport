package fr.takima.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EventForm {

    private List<Event> list;

    public EventForm() {
        this.list = new ArrayList<>();
    }

    public EventForm(List<Event> list) {
        this.list = list;
    }

    public List<Event> getList() {
        return list;
    }

    public void setList(List<Event> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "EventForm{" +
                "list=" + list +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EventForm eventForm = (EventForm) o;
        return Objects.equals(list, eventForm.list);
    }

    @Override
    public int hashCode() {
        return Objects.hash(list);
    }
}
