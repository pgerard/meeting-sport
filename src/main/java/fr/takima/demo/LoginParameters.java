package fr.takima.demo;

import java.util.Objects;

public class LoginParameters {
    private boolean canLogin;
    private long userId;

    public LoginParameters(boolean canLogin, long userId) {
        this.canLogin = canLogin;
        this.userId = userId;
    }

    public LoginParameters(){

    }

    @Override
    public String toString() {
        return "LoginParameters{" +
                "canLogin=" + canLogin +
                ", userId=" + userId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginParameters that = (LoginParameters) o;
        return canLogin == that.canLogin &&
                userId == that.userId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(canLogin, userId);
    }

    public boolean isCanLogin() {
        return canLogin;
    }

    public void setCanLogin(boolean canLogin) {
        this.canLogin = canLogin;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
