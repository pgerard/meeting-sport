package fr.takima.demo;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "events")
public class Event {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id private long id;
    @Column(name = "id_creator")
    private long idcreator;
    @Column(name = "month")
    private String month;
    @Column(name = "year")
    private int year;
    @Column(name = "day")
    private int day;
    @Column(name = "hour")
    private int hour;
    @Column(name = "minutes")
    private int minutes;
    @Column(name = "department")
    private String department;
    @Column(name = "place")
    private String place;
    @Column(name = "number_of_participant")
    private int numberOfParticipant;
    @Column(name = "type_of_sport")
    private String typeOfSport;
    @Column(name = "level")
    private String level;

    public Event(long id, long idcreator, String month, int year, int day, int hour, int minutes, String department, String place, int numberOfParticipant, String typeOfSport, String level) {
        this.id = id;
        this.idcreator = idcreator;
        this.month = month;
        this.year = year;
        this.day = day;
        this.hour = hour;
        this.minutes = minutes;
        this.department = department;
        this.place = place;
        this.numberOfParticipant = numberOfParticipant;
        this.typeOfSport = typeOfSport;
        this.level = level;
    }

    public Event() {

    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdcreator() {
        return idcreator;
    }

    public void setIdcreator(int idcreator) {
        this.idcreator = idcreator;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setIdcreator(long idcreator) {
        this.idcreator = idcreator;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public int getNumberOfParticipant() {
        return numberOfParticipant;
    }

    public void setNumberOfParticipant(int numberOfParticipant) {
        this.numberOfParticipant = numberOfParticipant;
    }

    public String getTypeOfSport() {
        return typeOfSport;
    }

    public void setTypeOfSport(String typeOfSport) {
        this.typeOfSport = typeOfSport;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", idcreator=" + idcreator +
                ", month='" + month + '\'' +
                ", year=" + year +
                ", day=" + day +
                ", hour=" + hour +
                ", minutes=" + minutes +
                ", department=" + department +
                ", place='" + place + '\'' +
                ", numberOfParticipant=" + numberOfParticipant +
                ", typeOfSport='" + typeOfSport + '\'' +
                ", level='" + level + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return id == event.id &&
                idcreator == event.idcreator &&
                year == event.year &&
                day == event.day &&
                hour == event.hour &&
                minutes == event.minutes &&
                department == event.department &&
                numberOfParticipant == event.numberOfParticipant &&
                Objects.equals(month, event.month) &&
                Objects.equals(place, event.place) &&
                Objects.equals(typeOfSport, event.typeOfSport) &&
                Objects.equals(level, event.level);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idcreator, month, year, day, hour, minutes, department, place, numberOfParticipant, typeOfSport, level);
    }
}
