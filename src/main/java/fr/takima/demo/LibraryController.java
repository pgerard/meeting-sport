package fr.takima.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.mail.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 *
 */
@RequestMapping("/")
@Controller
public class LibraryController {

    private final UserDAO userDAO;
    private final EventDAO eventDAO;

    @Autowired
    private JavaMailSender javaMailSender;

    public LibraryController(UserDAO userDAO, EventDAO eventDAO) {
        this.userDAO = userDAO;
        this.eventDAO = eventDAO;
    }

    @GetMapping
    public String homePage(Model m) {
        //m.addAttribute("users", userDAO.findAll());
        return "index";
    }

    @PostMapping
    public RedirectView connection(@ModelAttribute User user, HttpServletResponse response) {
        //stock en cookie l'ID utilisateur
        Cookie cookie = new Cookie("user_id", "1");
        response.addCookie(cookie);
        return new RedirectView("/search");
    }

    @GetMapping("/login")
    public String loginPage(Model m) {
        m.addAttribute("user", new User());
        return "login"; //.html
    }

    @PostMapping("/login")
    public RedirectView testLogin(@ModelAttribute User u, RedirectAttributes attributes, HttpServletResponse response) {
        Iterator<User> userTest = userDAO.findAll().iterator();
        String userEmail = u.getEmail();
        String userPassword = u.getPassword();

        //récupère paramètres: l'utilisateur peut se log ou non et si oui récupère le userID
        LoginParameters loginparameters = userCanLogin(userTest, userEmail, userPassword);
        if (loginparameters.isCanLogin()) {
            //stock en cookie l'ID utilisateur
            Cookie cookie = new Cookie("user_id", Long.toString(loginparameters.getUserId()));
            cookie.setMaxAge(24 * 60 * 60); // expire dans 24h
            response.addCookie(cookie);
            return new RedirectView("/search");
        } else {
            attributes.addFlashAttribute("message", "Un de vos identifiants est faux");
            return new RedirectView("/login");
        }
    }

    @GetMapping("/search")
    public String eventSearchPage(Model m, RedirectAttributes attributes ) {
        List<Event> list = new ArrayList<Event>();
        m.addAttribute("eventSelected", new Event());
        m.addAttribute("listEvent", list);
        return "search"; //.html
    }

    @PostMapping("/search")
    public RedirectView searchEvent(Model m, @ModelAttribute Event event, RedirectAttributes attributes, @RequestParam("valueSport") String valueSport) {
        List<Event> list = new ArrayList<Event>();
        Iterator<Event> allEvent = eventDAO.findAll().iterator();
        Event eventTampon;
        while (allEvent.hasNext()){
            eventTampon = allEvent.next();
            if(valueSport.equals(eventTampon.getTypeOfSport())){
                list.add(eventTampon);
            }
        }

        if (list.size()!=0){
            attributes.addAttribute("listevent",list);
            return new RedirectView("/eventFeed");
        }

        else {
            attributes.addFlashAttribute("message", "Aucun event trouvé.");
            return new RedirectView("/search");
        }

    }

    @GetMapping("/create")
    public String createEventPage(Model m) {
        m.addAttribute("event", new Event());
        return "createEvent"; //.html
    }

    @PostMapping("/create")
    public RedirectView submitEvent(@ModelAttribute Event event, RedirectAttributes attributes, HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            Cookie MonCookie = cookies[i];
            if (MonCookie.getName().equals("user_id")) {
                String Valeur = cookies[i].getValue();
                int userId = Integer.parseInt(Valeur);
                event.setIdcreator(userId);
            }
        }
        eventDAO.save(event);
        attributes.addFlashAttribute("message", "Évenement créé avec succès!");
        return new RedirectView("/search");
    }


    @GetMapping("/addUser")
    public String addUserPage(Model m) {
        m.addAttribute("user", new User());
        return "addUser";
    }

    @PostMapping("/addUser")
    public RedirectView submitAddUser(@ModelAttribute User user, RedirectAttributes attributes) {
        Iterator<User> allUsers = userDAO.findAll().iterator();
        String userTestEmail = user.getEmail(); //

        if (userAlreadyExists(allUsers, userTestEmail) == false) {
            userDAO.save(user);
            attributes.addFlashAttribute("message", "Utilisateur ajouté avec succès, connectez vous pour confirmer votre mail et password");
            return new RedirectView("/login");
        } else {
            attributes.addFlashAttribute("message", "Ce compte existe... ");
            return new RedirectView("/addUser");
        }
    }

    @GetMapping("/eventFeed")
    public String showEvent(Model m, @RequestParam("listevent") List<Event> list) {
        m.addAttribute("eventSelected", new Event());
        m.addAttribute("listEvent", list);
        return "eventFeed"; //.html
    }

    @PostMapping("/eventFeed")
    public RedirectView selectedEvent(@ModelAttribute Event eventselected, BindingResult bindingResult, @CookieValue("user_id") String userIdCookie, RedirectAttributes attributes) throws MessagingException {
        if(bindingResult.hasErrors()){
            attributes.addFlashAttribute("message", "Veuillez cocher une case avant de cliquer sur partciper!");
            return new RedirectView("/search");
        } else {
            Event eventParticipating = eventDAO.findById(eventselected.getId()).get();
            User eventCreator = userDAO.findById(eventParticipating.getIdcreator()).get();
            User activeUser = userDAO.findById(Long.valueOf(userIdCookie)).get();

            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setTo(eventCreator.getEmail());
            msg.setSubject("Quelqu'un est intéréssé par votre évênement !");
            msg.setText("Bonjour " + eventCreator.getFirstName() + "  " + eventCreator.getLastName() +
                    " \n " + activeUser.getFirstName() + " " + activeUser.getLastName() + " est intéressé par votre évênement" +
                    " " + eventParticipating.getTypeOfSport() + " à " + eventParticipating.getPlace() + " le " +
                    eventParticipating.getDay() + " " + eventParticipating.getMonth() + " " + eventParticipating.getYear() +
                    " à " + eventParticipating.getHour() + "h" + eventParticipating.getMinutes() + "\n Envoyez lui un message: \n"
                    + " téléphone: " + activeUser.getTelephoneNumber() + "\n" +
                    " email: " + activeUser.getEmail());

            javaMailSender.send(msg);

            attributes.addFlashAttribute("message", "Un mail à été envoyé aux créateur de l'évênement!");
            return new RedirectView("/search");
        }
    }


    @GetMapping("/parameters")
    public String modifyProfile(Model m, @CookieValue("user_id") String userIdCookie) {
        User myUser = userDAO.findById(Long.valueOf(userIdCookie)).get(); //get l'objet user de l'utilisateur actif
        m.addAttribute(myUser);
        return "parameters"; //.html
    }

    @PostMapping("/parameters")
    public RedirectView modifyProfile(@ModelAttribute User user, @CookieValue("user_id") String userIdCookie) {
        user.setId(Long.valueOf(userIdCookie));
        userDAO.save(user); //update
        return new RedirectView("/parameters");
    }

    @PostMapping("/deleteaccount")
    public RedirectView deleteUser(@CookieValue("user_id") String userIdCookie, HttpServletResponse response) {
        User activeUser = userDAO.findById(Long.valueOf(userIdCookie)).get(); //get l'objet user de l'utilisateur actif
        userDAO.delete(activeUser);
        Cookie session = new Cookie("user_id", null);
        session.setMaxAge(0);
        //add cookie to response
        response.addCookie(session);
        return new RedirectView("/");
    }

    @GetMapping("/myEvents")
    public String showEvent(Model m, RedirectAttributes attributes, @CookieValue("user_id") String userIdCookie, HttpServletRequest request) {

        Iterator<Event> allEvent = eventDAO.findAll().iterator();
        List<Event> list = new ArrayList<Event>();
        Event eventTampon; //prend la valeur de allEvent.next() pour  ne pas être utilisé 2 fois
        while (allEvent.hasNext()) {
            eventTampon = allEvent.next();
            if (Long.valueOf(userIdCookie).equals(eventTampon.getIdcreator())) {
                list.add(eventTampon);
            }
        }
        if (list.size()!= 0) {
            m.addAttribute("eventSelected", new Event());
            m.addAttribute("listEvent", list);
            return "myEvents";

        }
        else
        {
            m.addAttribute("message","Vous n'avez pas encore d'évènements créé, rendez-vous dans l'onglet créer un évènement");
            return "myEvents";
        }
    }

    @GetMapping("/logout")
    public RedirectView logoutUser(HttpServletResponse response) {
        Cookie session = new Cookie("user_id", null);
        session.setMaxAge(0);
        //add cookie to response
        response.addCookie(session);
        return new RedirectView("/");
    }

    @GetMapping("/modifiedEvent")
    public String modifiedEvent(Model m, @ModelAttribute Event eventSelected, BindingResult BindingResult, RedirectAttributes attributes) {
        if(BindingResult.hasErrors()){
            attributes.addFlashAttribute("message", "veuillez cocher une case avant de cliquer sur modifier!");
            return "/myEvents";
        } else{
            Event eventmodified = eventDAO.findById(eventSelected.getId()).get();
            m.addAttribute("eventModified", eventmodified);
            m.addAttribute("idEvent", eventmodified.getId());
            return "modifiedEvent";
        }

    }

    @PostMapping("/modifiedEvent")
    public RedirectView modifyEvent(@ModelAttribute Event eventModified, @CookieValue("user_id") String userIdCookie) {
        eventModified.setIdcreator((Long.valueOf(userIdCookie)));
        eventDAO.save(eventModified); //update
        return new RedirectView("/myEvents");
    }

    @PostMapping("/deleteEvent")
    public RedirectView deleteEvent(@ModelAttribute Event eventModified) {
        eventDAO.deleteById(eventModified.getId());
        return new RedirectView("/myEvents");
    }


    // METHODES //

    //méthode vérifiant si l'email et le mdp correspondent
    private LoginParameters userCanLogin(Iterator<User> allUsers, String userEmail, String userPassword) {
        boolean canLogin = false;
        long userId = 0;
        User userIterator;
        while (allUsers.hasNext()) { //Réaliser le test tant qu'il y a une donnée suivante
            userIterator = allUsers.next(); //Permet de tester la double connection
            if (userEmail.equals(userIterator.getEmail()) && userPassword.equals(userIterator.getPassword())) {
                userId = userIterator.getId();
                canLogin = true;
            }
        }
        return new LoginParameters(canLogin, userId);
    }

    //méthode vérifiant si un email existe déjà  dans la BDD
    private boolean userAlreadyExists(Iterator<User> allUsers, String userTestEmail) {
        boolean alreadyExists = false;
        while (allUsers.hasNext()) {
            if (userTestEmail.equals(allUsers.next().getEmail())) {
                alreadyExists = true;
            }
        }
        return alreadyExists;
    }
}
