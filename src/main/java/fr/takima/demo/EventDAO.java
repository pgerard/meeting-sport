package fr.takima.demo;

import org.springframework.data.repository.CrudRepository;

public interface EventDAO extends CrudRepository<Event, Long> {
}
